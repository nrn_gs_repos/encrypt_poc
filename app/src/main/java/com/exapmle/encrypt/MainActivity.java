package com.exapmle.encrypt;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends Activity {

    static String LogTag = "NJ_Log";
    final static String storyFileName = "anandi_anand";
    Button btn_Dec,btn_In;
    ProgressDialog progressDialogInMain;
    Context ctx;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;
        btn_Dec = (Button)findViewById(R.id.btn_Dec);
        btn_In = (Button)findViewById(R.id.btn_In);
        btn_Dec.setOnClickListener(btnDecListner);
        btn_In.setOnClickListener(btnInListner);
    }
    public OnClickListener btnInListner = new OnClickListener() {
        public void onClick(View v) {
            //VincentFileCrypto simpleCrypto = new VincentFileCrypto();
           // try {
                // encrypt audio file send as second argument and corresponding key in first argument.
//                encrypt = simpleCrypto.encrypt(KEY, getAudioFile());
//                //Store encrypted file in SD card of your mobile with name vincent.mp3.
//                FileOutputStream fos = new FileOutputStream(new File("/sdcard/vincent"));
//                fos.write(encrypt);
//                fos.close();
                showProgressDialog("Encrypting " + storyFileName + "...");
          //  new Handler().postDelayed(new Runnable() {
          //      @Override//     public void run() {

                    Thread encryptionThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                encryptMyFile();
                            } catch (Exception e) {
                                logExceptionStackTrace(e);
                            }
                        }
                    });

                    encryptionThread.start();
           //     }
          //  }, 2000);







        }
    };

    public void logExceptionStackTrace(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String msg = sw.toString();
        Log.e(LogTag, msg);
        dismissMyProgressDialog();
        showToastWithMessage("Error : "+ e.getLocalizedMessage());
    }

    public void showToastWithMessage(final String msg){
        (MainActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public OnClickListener btnDecListner = new OnClickListener() {
        public void onClick(View v) {
           // VincentFileCrypto simpleCrypto = new VincentFileCrypto();

                // decrypt the file here first argument is key and second is encrypted file which we get from SD card.
//                decrpt = simpleCrypto.decrypt(KEY, getAudioFileFromSdCard());
//                Log.d("decrypt ", "decrpted : " + decrpt);
//                FileOutputStream fos = new FileOutputStream(new File("/sdcard/vincent"));
//                fos.write(decrpt);
//                fos.close();
            showProgressDialog("Decrypting "+storyFileName+" ...");
                Thread decryptionThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                            try {
                                decryptMyFile();
                            } catch (Exception e) {
                                logExceptionStackTrace(e);
                            }
                    }
                });
            decryptionThread.start();

                //play decrypted audio file.
               // playMp3(decrpt);

        }
    };

    void showProgressDialog(String msg) {
        Log.d(MainActivity.LogTag, msg);
        if (progressDialogInMain == null) {
            progressDialogInMain = new ProgressDialog(MainActivity.this);
        }
        progressDialogInMain.setMessage(msg);
        progressDialogInMain.setCancelable(false);
        progressDialogInMain.show();
    }

    // decr
// IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSize

// enc
//  IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKey
     void encryptMyFile() throws Exception {
        // Here you read the cleartext.
        File extStore = Environment.getExternalStorageDirectory();
//        FileInputStream fis = new FileInputStream(extStore + "/sampleFile");
        AssetManager am = ctx.getAssets();
        InputStream is = am.open(storyFileName + ".mp3");
        // This stream write the encrypted text. This stream will be wrapped by
        // another stream.
        FileOutputStream fos = new FileOutputStream(createFileUnderGoStoriesDir(storyFileName));
        // Length is 16 byte
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(),
                "AES");
        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[1024*8];
        while ((b = is.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        is.close();
       //  Toast.makeText(getApplicationContext(), "encrytion complete", Toast.LENGTH_SHORT).show();

         dismissMyProgressDialog();
         showToastWithMessage("Encryption completed");
     }

    public void dismissMyProgressDialog(){
        (MainActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialogInMain != null) {
                    progressDialogInMain.dismiss();
                }
            }
        });
    }
    public File createFileUnderGoStoriesDir(String fileName)
    {
        File fileWithGivenName = null;
        try{
            File GoStoriesDir = new File(Environment.getExternalStorageDirectory().getPath()+ File.separator+"NishEncStories");
            if (!GoStoriesDir.exists()) {
                if(GoStoriesDir.mkdir()){
                    Log.e(MainActivity.LogTag, "\n NishEncStories directory creation SUCCESS \n");
                }else {
                    Log.e(MainActivity.LogTag, "\n NishEncStories directory creation FAILED \n");
                }
            }
            fileWithGivenName = new File(GoStoriesDir+ File.separator+fileName);

            if(fileWithGivenName.exists()){
                Log.e(MainActivity.LogTag, "\n fileWithGivenName created \n");
            }else {
                if (fileWithGivenName.createNewFile()) {
                    Log.e(MainActivity.LogTag, "\n fileWithGivenName creation SUCCESS \n");
                }else {
                    Log.e(MainActivity.LogTag, "\n fileWithGivenName creation FAILED \n");
                }
            }
        }catch (Exception e){
            logExceptionStackTrace(e);

        }
        return fileWithGivenName;
    }


    void decryptMyFile() throws Exception {

        File extStore = Environment.getExternalStorageDirectory();
        FileInputStream fis = new FileInputStream(extStore + "/NishEncStories/" + storyFileName);
        FileOutputStream fos = new FileOutputStream(extStore + "/NishEncStories" +"/"+storyFileName+".mp3");
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(),
                "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);

        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        int start = 0;
        int i = 0;
         byte[] d = new byte[1024*8];
        byte[] mp = new byte[0];
         ByteArrayOutputStream os = new ByteArrayOutputStream();
        while ((b = cis.read(d)) != -1) {
            i++;
            fos.write(d, 0, b);
        //    os.write(d, 0, b);
        //    byte[] decrypted = cipher.doFinal(d);
         //   mp[start + i] = d[b];
            Log.d(LogTag , "mp bytes  " + b+ "\n d " + d + "\n cis.read(d) " + cis);
        }
        byte[] decrypted = os.toByteArray();
        Log.d("decrypted ", "decrypted    " + decrypted);
        fos.flush();
        fos.close();
        os.close();
        cis.close();
        // Toast.makeText(getApplicationContext(), "decrytion complete", Toast.LENGTH_SHORT).show();
        //playMp3(decrypted);
        dismissMyProgressDialog();
        showToastWithMessage("Decryption completed");
    }
    private void playMp3(byte[] mp3SoundByteArray) {
        try {
            File tempMp3 = File.createTempFile("kurchina", "mp3", getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(mp3SoundByteArray);
            fos.close();
            MediaPlayer mediaPlayer = new MediaPlayer();
            FileInputStream fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            logExceptionStackTrace(e);
        }
    }

    /**
     *
     * @return byte array for encryption.
     * @throws FileNotFoundException
     */
/*    public byte[] getAudioFile() throws FileNotFoundException
    {
        byte[] audio_data = null;
        byte[] inarry = null;
        AssetManager am = ctx.getAssets();
        try {
            InputStream is = am.open("jaiambetuhai.lite.mp3"); // use recorded file instead of getting file from assets folder.
            int length = is.available();
            audio_data = new byte[length];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while ((bytesRead = is.read(audio_data)) != -1)
            {
                output.write(audio_data, 0, bytesRead);
            }
            inarry = output.toByteArray();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return inarry;
    }*/
    /**

     * This method fetch encrypted file which is save in sd card and convert it in byte array after that this  file will be decrept.

     * @return byte array of encrypted data for decription.

     * @throws FileNotFoundException

     */
/*    public byte[] getAudioFileFromSdCard() throws FileNotFoundException
    {
        byte[] inarry = null;
        try {
            //getting root path where encrypted file is stored.
            File sdcard  = Environment.getExternalStorageDirectory();
            File file = new File("/sdcard/vincent"); //Creating file object
            //Convert file into array of bytes.
            FileInputStream fileInputStream=null;
            byte[] bFile = new byte[(int) file.length()];
            Log.d("file length  ", "length  " + String.valueOf(file.length()));
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            inarry = bFile;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d("inarry", "inarry : " + inarry);
        return inarry;
    }*/
    /**

     * This Method is used to play audio file after decrepting.

     * @param mp3SoundByteArray : This is our audio file which will be play and it converted in byte array.

     */
  /*  private void playMp3(byte[] mp3SoundByteArray) {
        try {
            // create temp file that will hold byte array
            File tempMp3 = File.createTempFile("kurchina", "mp3", getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(mp3SoundByteArray);
            fos.close();
            // Tried reusing instance of media player
            // but that resulted in system crashes...
            MediaPlayer mediaPlayer = new MediaPlayer();
            FileInputStream fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }*/
}